#! /usr/bin/env python

#Copyright <2018> <RIZWIN ANTONY>

#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import rospy
# Brings in the SimpleActionClient
import actionlib
# Brings in the messages used by the person action, including the
# goal message and the result message.
import person.msg
   

class dataAction(object):
    # create messages that are used to publish feedback/result
    _feedback = person.msg.dataFeedback()
    _result = person.msg.dataResult()
    #SimpleActionServer is created
    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, person.msg.dataAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
    #execute callback function that we'll run everytime a new goal is received  
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # let the user know that the action is executing. 
        rospy.loginfo('searching the details of  %s',goal.name)
        x=goal.name
       
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
          
        #create dictionary containing persons age     
        person_details={'john': 22, 'rizwin':24, 'adam':22, 'brad': 30,'pepe':34}
        ## start executing the action
        
        for i in person_details:
                if i==x:
                         p=0
                         p=person_details[i]
                         self._feedback.sequence= p
                         self._as.publish_feedback(self._feedback)
                         break
                else:
                         f=0 
                         self._feedback.sequence=f 
                         self._as.publish_feedback(self._feedback)
                    
                
        # this step is not necessary, the sequence is computed at 1 Hz for demonstration purpose
        r.sleep()
          
        if success:
            #action server notifies the action client that the goal is complete
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)

#start of main function        
if __name__ == '__main__':
    
    rospy.init_node('data_server')
    # creates the action server
    server = dataAction(rospy.get_name())
    rospy.spin()
